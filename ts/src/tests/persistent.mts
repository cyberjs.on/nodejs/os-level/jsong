import { Cli } from '../index.mjs';


const cli = new Cli;

cli.wait4EntryKey('escape');

cli.catch(
    {
        label: 'Entre com uma frase ou palavra... '
        , exitWords: ['go out', 'quit']
        // , keepAlive: false
    },
    typed => {
        console.log('\nTyped:', typed, '\n');
    }
);
