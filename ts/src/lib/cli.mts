import NodeReadline from 'node:readline';
// import NodeReadlinePromises from 'node:readline/promises';

import { PersistenceOptions } from './types/index.mjs';


export class Cli {

    #readLine: NodeReadline.Interface;

    // #readLinePromise: NodeReadlinePromises.Interface;

    constructor() {

        const settings = {
            input: process.stdin,
            output: process.stdout
        };

        this.#readLine = NodeReadline.createInterface(settings);

        /**
         *
         * se ambos forem criados, começa-se a dar problema com duplicidade de caracter digitado. Logo, ou se deve manter assim, ou criar um objeto
         * separado só parar tratar as entradas com Promises
         */
        // this.#readLinePromise = NodeReadlinePromises.createInterface(settings);
    }

    catch(label: string): Promise<string>;
    catch(label: string, callback: (input: string) => void): void;
    catch(settingOptions: PersistenceOptions, callback: (input: string) => void): void;
    catch(settingOptions: string | PersistenceOptions, callback?: (input: string) => void) {

        let persistenseOptions: PersistenceOptions;

        let promise: Promise<string>;

        let label: string;

        if (callback) {

            if (typeof arguments[0] === 'string') {
                label = arguments[0];
                persistenseOptions = {label}
            } else {
                persistenseOptions = settingOptions as PersistenceOptions;
            }

            label = persistenseOptions.label;

            this.#readLine.question(
                label,
                (input: string) => {

                    if (!Reflect.has(persistenseOptions, 'exitWords')) {
                        persistenseOptions.exitWords = ['go out'];
                    } else if (typeof persistenseOptions.exitWords === 'string') {
                        persistenseOptions.exitWords = [persistenseOptions.exitWords];
                    }

                    if ((persistenseOptions.exitWords as string[]).includes(input)) {
                        return this.#readLine.close();
                    } else {
                        callback(input);
                        this.catch(persistenseOptions, callback);
                    }

                }
            );
        } else {
            label = settingOptions as string;

            promise = new Promise<string>(
                (resolve) => {

                    this.#readLine.question(
                        label,
                        (internalInput: string) => {
                            resolve(internalInput);
                        }
                    );
                }
            );

            return promise;

            // return this.#readLinePromise.question(label);
        }

    }

    wait4EntryKey(exitKeyname?: string) {

        NodeReadline.emitKeypressEvents(process.stdin);

        if (process.stdin.isTTY) {
            process.stdin.setRawMode(true);
        }

        process.stdin.on('keypress', (chunk, key) => {
            if (!exitKeyname || key.name === exitKeyname)
                process.exit();
            }
        );
    }

}
