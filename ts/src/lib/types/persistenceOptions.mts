export type PersistenceOptions = {

    label: string;

    exitWords?: string | string[];

}
